package com.qatesting;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
//import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class PlayStore {
	WebDriver driver;

	@BeforeTest
	public void start() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Mira\\eclipse-workspace\\qa-challenge\\chromedriver.exe");

		driver = new ChromeDriver();
		driver.get("https://play.google.com/store?hl=en");
		driver.manage().window().maximize();
	}

	@Test
	public void Login() throws InterruptedException {
		WebElement sign_in = driver.findElement(By.id("gb_70"));
		sign_in.click();
		Thread.sleep(6000);
		driver.findElement(By.id("identifierId")).click();
		driver.findElement(By.id("identifierId")).clear();
		driver.findElement(By.id("identifierId")).sendKeys("miraremon34@gmail.com");
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Learn more'])[1]/following::span[1]"))
				.click();
		Thread.sleep(2000);
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys("mira12345678");
		driver.findElement(By.name("password")).sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Collections'])[1]/following::span[1]"))
				.click();
		Assert.assertEquals(driver
				.findElement(By
						.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Change'])[1]/following::div[2]"))
				.getText(), "mira remon");
		Assert.assertEquals(driver
				.findElement(By.xpath(
						"(.//*[normalize-space(text()) and normalize-space(.)='mira remon'])[1]/following::div[1]"))
				.getText(), "miraremon34@gmail.com");
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Collections'])[1]/following::span[1]"))
				.click();

	}

	@Test
	public void add_wishlist() throws InterruptedException {
	 driver.findElement(By
	  .xpath("(.//*[normalize-space(text()) and normalize-space(.)='Entertainment'])[1]/following::span[4]"))
	  .click();
	  Thread.sleep(3000);
	 
	  driver.findElement(By.linkText("Games")).click();
	  Thread.sleep(3000);
	 
	  driver.findElement(By.linkText("See more")).click();
	  Thread.sleep(3000);
	 
	  driver.findElement(By.linkText("Cooking Fever")).click();
	  driver.findElement(By.xpath(
	  "//*[@id=\"fcxH9b\"]/div[4]/c-wiz/div/div[2]/div/div[1]/div/c-wiz[1]/c-wiz[1]/div/div[2]/div/div[2]/div/div[2]/div[1]/c-wiz/wishlist-add/button"))
	  .click();
	  Thread.sleep(3000);
	 
	  driver.navigate().back();
	  Thread.sleep(3000);
	  JavascriptExecutor js = (JavascriptExecutor) driver;
	  js.executeScript("window.scrollBy(0,3000)");
	  Thread.sleep(3000);
	  driver.findElement(By.linkText("Guns of Boom - Online PvP Action")).click();
	  Thread.sleep(3000);
	 
	  driver.findElement(By.xpath(
	  "//*[@id=\"fcxH9b\"]/div[4]/c-wiz/div/div[2]/div/div[1]/div/c-wiz[1]/c-wiz[1]/div/div[2]/div/div[2]/div/div[2]/div[1]/c-wiz/wishlist-add/button"))
	.click();
	 Thread.sleep(3000);
	 driver.findElement(By.linkText("My wishlist")).click();
	 Thread.sleep(3000);
	 Assert.assertEquals(driver.findElement(By.linkText("Guns of Boom - Online PvP Action")).getText(),"Guns of Boom - Online PvP Action");
	 Assert.assertEquals(driver.findElement(By.linkText("Cooking Fever")).getText(), "Cooking Fever"); 
	 driver.findElement(By.linkText("Cooking Fever")).click();
	 Thread.sleep(3000);
	 driver.findElement(By.xpath( ("//*[@id=\"fcxH9b\"]/div[4]/c-wiz/div/div[2]/div/div[1]/div/c-wiz[1]/c-wiz[1]/div/div[2]/div/div[2]/div/div[2]/div[2]/c-wiz/div/span/button")))
	 .click();
	 driver.get( "https://accounts.google.com/signin/v2/sl/pwd?service=googleplay&passive=0&authuser=0&continue=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dcom.nordcurrent.canteenhd%26rdid%3Dcom.nordcurrent.canteenhd&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
	 Thread.sleep(1000);
	 driver.findElement(By.name("password")).clear();
	 driver.findElement(By.name("password")).sendKeys("mira12345678");
	 driver.findElement(By.name("password")).sendKeys(Keys.ENTER);
	 }

	@Test
	public void write_review() throws InterruptedException {
		driver.get("https://play.google.com/store/apps/details?id=com.kiloo.subwaysurf&hl=en");
		Thread.sleep(1000);

		driver.findElement(By
				.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Review Policy'])[1]/following::span[3]"));
		
		driver.get("https://play.google.com/store/ereview?docId=com.kiloo.subwaysurf&hl=en&width=936&source=play-store-boq-web&usegapi=1&id=I4_1542789925614&_gfid=I4_1542789925614&parent=https://play.google.com&pfname&rphttps://play.google.com/store/ereview?docId=com.kiloo.subwaysurf&hl=en&width=936&source=play-store-boq-web&usegapi=1&id=I4_1542789925614&_gfid=I4_1542789925614&parent=https://play.google.com&pfname&rpctoken=19174334&jsh=m;/_/scs/abc-static/_/js/k%3Dgapi.gapi.en.CUp85wbT4DI.O/rt%3Dj/d%3D1/rs%3DAHpOoo-XBQda2DFvo9hxbj_dGnCV84SJMA/m%3D__features__ctoken=19174334&jsh=m;/_/scs/abc-static/_/js/k%3Dgapi.gapi.en.CUp85wbT4DI.O/rt%3Dj/d%3D1/rs%3DAHpOoo-XBQda2DFvo9hxbj_dGnCV84SJMA/m%3D__features__");
		Thread.sleep(5000);

	 
	    driver.findElement(By.cssSelector("div.review-row.star-rating-row-desktop > div.review-stars-container > div.write-star-rating-container > div.medium-star-material > div.star-rating-editable-container > span.fifth-star.star-common"));	

	    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Kiloo'])[1]/following::textarea[1]")).sendKeys("This game is one of the best");

	    Thread.sleep(3000);

		driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Loved it'])[2]/following::button[1]")).click();
		Thread.sleep(3000);
		  driver.navigate().back();

	}

	@AfterClass
	public void cleaupProc() {
		System.out.print("\nDone");
		driver.quit();
	}

}
