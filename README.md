
# QA Challenge

1- Clone this repo and create an Automated test including Test cases (Added within coding) for Installation and review of any game from play-store with Selenium - Java using TestNG framework.
(Done)


Steps: 
1-	@Before Test , Login() -> Open website https://play.google.com/ from chrome with Gmail account and login and check that we logged in successfully

2-	Add_Wishlist() add to From home, Open and select to add the first and last game displayed to the wishlist. 

3-	Open My wishlist, open one of the games, and install it. 

4-	Verify password and continue.

5-	Choose a mobile device and continue.

6-	Wait for the installation to complete on the device (Hint: you can use Vysor to indicate this, or any other way you see fit).

7-	write_review() Back to the opened page, “Write a review” button will be displayed, press it. 

8-	Select to continue, select 5 stars, add Test in review text field (after not before the stars selection) and submit your review.

9-	Back to the page, select to delete the review added.

Please submit the above requirements to gitlab and make it executable through gitlab CI/CD feature including a brief documentation / readme of your project. 

Regarding CI/CD I created the .gitlab-ci.yml file with the commands needed to make it executable and I pushed it on gitlab


